### plotting.py

# This function provides a visualization of the studied geometry
# On the fly plotting is slow and intended for debug purposes only

def plotGeometry(OPT, pl):
    # Customize 3D view
    if (OPT['plot']):
        from yade import qt, Vector3
        from yade.utils import aabbExtrema
        import os.path
        mn,mx = aabbExtrema()
        lookAt = 0.5 * (mx + mn)
        rr = pl[0]; vv = pl[1]; cc = pl[2]
        rr.bgColor = Vector3(1., 1., 1.)
        rr.light1 = True
        rr.light2 = True
        rr.ghosts = False
        rr.light2Color = Vector3(0.2, 0., 0.)
        vv.sceneRadius = 4 * lookAt[2]
        vv.showEntireScene = True
        vv.eyePosition = 1.2 * Vector3(lookAt[2], lookAt[2], 0)
        vv.lookAt = lookAt
        if (OPT['saveSnaps']):
            path = os.getcwd() + OPT['wdir']
            if ( not os.path.exists( path )):
                os.mkdir( path )
            path = path + OPT['nrun'] + '/'
            if ( not os.path.exists( path )):
                os.mkdir( path )
            for i in range(10): vv.saveSnapshot(path + OPT['nrun'] + "_snapshot.jpg")
    return O
