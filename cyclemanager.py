### cyclemanager.py
from waveaux import *
from fft import dftSolver
from yade import Vector3
from yade.utils import setRefSe3

# This function defines the stages of the numerical experiment and calls appropriate functions

def cycleManager(O, RVE, TEST, OPT, AUX, dft, ufft):

        relaxationSteps = TEST['relaxationSteps']
        agitationSteps = TEST['agitationSteps']
        propagationSteps = TEST['propagationSteps']
        printEvery = TEST['printEvery']
        saveEvery = TEST['saveEvery']
        dftEvery = TEST['dftEvery']
        relaxation_timestep = TEST['REL_TS']
        experiment_timestep = TEST['EX_TS']

        # Prepare geometry
        geometry = setGage(O, RVE['props']['subcell_size'])

        if AUX['stage'] == 'Relaxation' and O.iter == 1:
            O = init_test(O, geometry)
            O.engines[-2].damping = TEST['relaxationDamping']

        if (O.iter == 1): # Initialization
            if OPT['saveDFT']: dft.reinit(O, TEST, OPT, RVE)
            if OPT['saveUFFT']: ufft.reinit(O, TEST, OPT, RVE)


        if nth(O.iter, printEvery):
                if OPT['verbose']: print(clr.END + clr.BOLD + clr.WHITE, AUX['stage'], "(", O.iter, " iterations)")

        if AUX['stage'] == 'Agitation':
                agitateWave(O, geometry, TEST, OPT)

        if AUX['stage'] == 'Propagation' and nth(O.iter, saveEvery):
                savePropData(O, OPT)
                #saveVelocityProfile(O, TEST, OPT)
                #addPosMagSample(O, TEST, OPT, AUX)
                if OPT['functional']=='kin_int':
                    addKineticEnergy(O, RVE, AUX)

        if AUX['stage'] == 'Propagation' and nth(O.iter, dftEvery):
                if OPT['saveDFT'] and O.iter > TEST['startFFTStep']: dft.on_the_fly(O)
                if OPT['saveUFFT'] and O.iter > TEST['startFFTStep']: ufft.on_the_fly(O)


        if AUX['stage'] == 'Relaxation' and O.iter == relaxationSteps: # Switch R->A
                O.resetTime()
                O.dt = TEST['EX_TS'];
                O.engines[-2].damping = 0.0;
                setRefSe3();
                if OPT['verbose']: print(clr.END + clr.BOLD + clr.GREEN, "Starting agitation stage")
                AUX['stage'] = 'Agitation'
                O.run(agitationSteps+1)

        if AUX['stage'] == 'Agitation' and O.iter == agitationSteps: # Swich A->P
                O.resetTime()
                if OPT['verbose']: print(clr.END + clr.BOLD + clr.GREEN, "Starting propagation stage")
                AUX['stage'] = 'Propagation'
                O.run(propagationSteps+1)

        if AUX['stage'] == 'Propagation' and O.iter == propagationSteps: # Final

                #AUX['peakVel'], AUX['peakAtt'] = computeVelMag(O, AUX, OPT)
                #if OPT['functional']=='kin_int':
                #    normalizeKineticEnergy(AUX)
                AUX = finalSave(OPT, AUX, dft, ufft)
                AUX['stage'] = 'Done'


        return AUX

