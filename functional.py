### functional.py

# In this file we define the functional based on the resulf of our simulation
# That can be minimized by the extermal minimization procedure

import numpy as np
from color_lib import clr

def functional(RVE, OPT, AUX):

    # Simple toy functional
    if (OPT['functional']=='toy'):
        functional = (RVE['props']['N_x'] - 5)**2 + (RVE['props']['N_y'] - 5)**2 + (RVE['props']['N_z'] - 5)**2

    # Peak velocity
    if (OPT['functional']=='peakVel'):
        functional = AUX['peakVel']

    # Attenuation coefficient
    if (OPT['functional']=='peakAtt'):
        functional = AUX['peakAtt']

    # Velocity and attenuation
    if (OPT['functional']=='VelAttTuple'):
        functional = AUX['peakVel'], AUX['peakAtt']

    # Peak velocity + velocity from spectrum
    if (OPT['functional']=='Vel2'):
        functional = AUX['peakVel'], AUX['fftVel']

    # Velocity and attenuation
    if (OPT['functional']=='fftVelAtt'):
        functional = AUX['fftVel'], AUX['fftAtt'], AUX['fftAtt2']

    # Kinetic energy functional
    if (OPT['functional']=='combined'):
        functional = [AUX['kinetic_integral'],
                      AUX['band_1d'],
                      AUX['band_2d'],
                      AUX['band_3d'],
                      AUX['band_4d'],
                      AUX['band_5d'],
                      AUX['band_1f'],
                      AUX['band_2f'],
                      AUX['band_3f'],
                      AUX['band_4f'],
                      AUX['band_5f']
                      ]
        AUX['kinetic_integral'] = 0.0 # Make sure that if the calculation was not finished, we'll get zeros and not the previous step value
        AUX['band_1d'] = 0.0
        AUX['band_2d'] = 0.0
        AUX['band_3d'] = 0.0
        AUX['band_4d'] = 0.0
        AUX['band_5d'] = 0.0
        AUX['band_1f'] = 0.0
        AUX['band_2f'] = 0.0
        AUX['band_3f'] = 0.0
        AUX['band_4f'] = 0.0
        AUX['band_5f'] = 0.0

    # Band energy functional
    if (OPT['functional']=='band_en'):
        functional = [AUX['band_energy']]

    if (OPT['verbose']): print(clr.END + clr.BOLD + clr.BLUE + "Functional value:", functional)

    return functional
