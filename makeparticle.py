###  makeparticle.py

# This procedure appends a sphere to O.bodies

def makeParticle(kind, coords, rad, O, MAT):
    from yade.utils import sphere
    O.bodies.append([sphere(center=(coords[0], coords[1], coords[2]), \
                                radius=rad, \
                                color=MAT['mat_array'][kind]['color'], \
                                material=MAT['mat_array'][kind]['label'])])
    return O

