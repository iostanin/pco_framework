### baseline.py

# This file sets the most important parameters of the test.
# Baseline settings are stored in a set of dictionaries:
#   RVE: parameters of an RVE studied
#   MAT: parameters of materials
#   TEST: parameters of the test
#   OPT: simulation options
#   AUX: additional parameters
# After the baseline settings are initialized, they can be
# corrected by the batch runner (linear sweep) or optimization
# procedure.

import numpy as np
from color_lib import clr

def phononic_test_baseline():

    ##----------------------------------------------------------------------
    ## 1) Properties of an RVE (here - a regular column aligned with z axis)
    ##----------------------------------------------------------------------

    # Face-Centered Cubic RVE (can be replaced with the other type of arrangement).
    FCC_props = {'N_x': 3,              # Cells along x
                 'N_y': 3,              # Cells along y

                 'N_b': 2,              # Buffer layers separating 1st grip and PC
                 'N_c': 8,              # Number of layers in phononic crystal
                 'N_d': 30,             # Number of layers in detector

                 'PartPerCell': 4,      # Particles per cell
                 'subcell_size': 0.2,   # Size of a single FCC cell
                 'type': 1}             # Default particle type

    FCC_props['N_z'] = FCC_props['N_b'] + FCC_props['N_c'] + FCC_props['N_d'] + 2

    # Some useful quantities
    Nx = FCC_props['N_x']
    Ny = FCC_props['N_y']
    Nc = FCC_props['N_c']
    PartPerCell = FCC_props['PartPerCell']
    PartPerKind = Nx * Ny * PartPerCell         # Number of particles in a single slice
    PartTotal = PartPerKind * Nc                # Total number of particles in RVE

    # Array of particle types
    ParType = np.zeros([PartTotal])

    # General RVE properties
    RVE = {'type': "FCC_layer",       # FCC_layer for layered Ph.Cr., FCC_rand for random Ph.Cr.
           'props': FCC_props,       # Set of properties, depending on type of RVE
           'margin': -0.0442290,        # Way to introduce pre-strain and save time
           'sigmaIso': -1e9,         # Isotropic compressive stess
           'damping': 0.9,           # RVE relaxation damping
           'RVE_TS': 2.0e-05,        # RVE relaxation timestep
          }

    if (RVE['type'])=="FCC_rand": # Randomly shuffle particle types within phononic crystal regions
        kind = 0
        for l in range(Nc):
            kind = kind+1
            for k in range(PartPerKind):
                ok = 0
                while not ok:
                    ind = np.random.randint(0, PartTotal)
                    if not ParType[ind]:
                        ParType[ind] = kind
                        ok = 1
        RVE['placement'] = ParType

    ##----------------------------------------------------------------------
    ## 2) Simulation materials
    ##----------------------------------------------------------------------

    # Default material
    base_material  = {
                 'young': 1E11,            # Young's modulus [Pa]
                 'poiss': 0.2,             # Poisson's ratio
                 'frict': 0.0,             # Friction coefficient
                 'dens' : 10000,           # Density [kg/m^3]
                 'label': 'base',          # Yade Label
                 'color': [0.8, 0.8, 0.8]  # Color used in yade visualization
                 }

    MAT = {
            'mat_array' : [base_material], # Array of materials used in simulation
            'max_dens': 16000,             # Maximum allowed density (for density optimization procedure)
            'min_dens': 4000,              # Minimum allowed density (for density optimization procedure)
            'model_type': 'Hertz'          # Either 'Linear' for linear Cundall model or 'Hertz' for Hertz-Mindlin model
    }

    # Fill materials array with the instances of default material. Material parameters can be changed only at the stage of initialization!
    # Changing material during simulation will not have effect

    for i in range(FCC_props['N_c']):
        mat = {
        'young': 1E11,                                                   # Young's modulus [Pa]
        'poiss': 0.2,                                                    # Poisson's ratio
        'frict': 0.0,                                                    # Friction coefficient
        'dens' : 10000,                                                  # Density [kg/m^3]
        'label': 'mat_' + str(i),                                        # Yade Label
        'color': [np.random.rand(), np.random.rand(), np.random.rand()]  # Color used in yade visualization
        }
        MAT['mat_array'].append(mat)

    ##----------------------------------------------------------------------
    ## 3) Parameters of the acoustic test
    ##----------------------------------------------------------------------

    TEST = {
             'relaxationSteps'  : 100,        # Timesteps of relaxation with grips installed
             'relaxationDamping': 0.9,          # Pre-test rel.damping. Useful to have it higher than RVE damp.
             'agitationSteps'   : 100,         # Timesteps of wave agitation (single sin/cos period or meander pulse)
             'propagationSteps' : 14000,        # Timesteps of wave propagation
             'startFFTStep'     : 4000,         # Time when FFT summation starts
             'printEvery'       : 200,           # Test outputs frequency
             'saveEvery'        : 200,           # Save configuration every Nth cycle
             'dftEvery'         : 200,           # Perform DFT computations Nth cycle
             'REL_TS'           : 2.0e-05,      # Column relaxation timestep
             'EX_TS'            : 4.0e-07,      # Timestep
             'waveShape'        : 'sin',        # Shape of agitated signal: 'sin','cos','lin'
             'waveType'         : 'P',          # Wave type: 'P' - for p-wave, 'S' - for s-wave.
             'waveMag'          : 0.00001
            }

    ##----------------------------------------------------------------------
    ## 4) Simulation options/preferences
    ##----------------------------------------------------------------------

    OPT = {
             'dftMode'   : 'detector',            # 'detector', 'all' - defines spatial region used for FFT
             'saveUFFT'  : True,                  # Perform layer-wise fast Fourier transform
             'saveDFT'   : False,                  # Perform all-particle Fourier transform
             'saveVTK'   : True,                  # Save intermediate configurations
             'saveSnaps' : False,                 # Save viewport images at every stage
             'saveStates': True,                  # Save yade states
             'saveProf'  : True,                  # Save velocity profiles
             'verbose'   : True,                  # Detailed console text messages
             'iterInfo'  : False,                 # Text messages from optimizer/batch runner
             'plot'      : False,                 # On the fly visualization
             'peakMon'   : True,                  # Indicate peak position and velocity on the fly
             'functional': 'combined',             # Type of the functional to return 'peakVel', etc.
             'wdir'       : '/optimization/',      # Home directory for runs (local path w/r to current working dir)
             'nrun'       : 'run_01',              # Name of the run
             'zeropath'  : ''                     # Place to store zero configuration
          }

    # Auxiliary global variables
    AUX = {
           'isPeak'             : True,                         # Is peak observed at the snapshot
           'SamplesCollected'   : 0,                            # Number of peak positions that are used to determine velocity and attenuation
           'peakPosition'       : np.array([0.0]),              # Samples with peak positions
           'peakMagnitude'      : np.array([0.0]),              # Samples with peak magnitudes
           'peakMoment'         : np.array([0.0]),

           # Lorenzian(w) = (S0 * gamma(k)) / ((w - Omega(k))^2 + gamma(k)^2)

           'SArray'             : np.array([0.0]),              # Set of values of magnitude (S0) of Lorenzian (should be constant)
           'WArray'             : np.array([0.0]),              # Set of values of mean (Omega(k)) of Lorenzian
           'GArray'             : np.array([0.0]),              # Set of values of dispersion (gamma(k)) of Lorenzian
           'peakVel'            : 0.0,                          # Peak velocity derived from time-domain (Least Squares)
           'peakAtt'            : 0.0,                          # Peak attenuation magnitude (Least Squares)
           'fftVel'             : 0.0,                          # Found as the linear approximation
           'fftAtt'             : 0.0,                          # Found as the mean dispersion of Lorenzian function over all k-s
           'fftAtt2'            : 0.0,                          # Low-freq part of attenuation coefficient
           'funValue'           : 0.0,
           'kinetic_integral'   : 0.0,
           'k_min'              : 0.0,
           'k_max'              : 0.5,
           'w_min'              : 0.0,
           'w_max'              : 1.0,
           'band_energy'        : 0.0,
           'band_1d'             : 0.0,
           'band_2d'             : 0.0,
           'band_3d'             : 0.0,
           'band_4d'             : 0.0,
           'band_5d'             : 0.0,
           'band_1f'             : 0.0,
           'band_2f'             : 0.0,
           'band_3f'             : 0.0,
           'band_4f'             : 0.0,
           'band_5f'             : 0.0,
           'band_mode'          : 'min',
           'kinetic_integral_0' : 1.0,

           'stage'              : 'Assembly'                   # Assembly, Relaxation, Agitation, Propagation, Done - used by cycle manager
           }

    if OPT['verbose']: print(clr.END + clr.VIOLET + clr.BOLD + "Baseline test parameters loaded")
    return RVE, MAT, TEST, OPT, AUX
