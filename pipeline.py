### pipeline.py

# This script defines the pipeline function

# The pipeline does the following sequence of actions
# 1) creates an RVE according to one of prescribed types,
# 2) compresses it to a desirable state of stress,
# 3) clones RVE few times along z axis to create a long column,
# 4) introduces additional features (layers, point defects),
# 5) fixes grips along z direction, while preserving transvers. PBC,
# 6) agitates wave of a prescribed type,
# 7) performs a time and frequency domain of the wave propagation,
# 8) performs post-processing and saving of results,
# 9) returns a value of a selected functional (e.g. wave velocity).
# The pipeline can be used in
# 1) Single run mode (see file main_singlerun)
# 2) Batch mode (see file main_batch)
# 3) Optimization mode (see file main_optimization)

import numpy as np
from generatearrangement import *
from creatematerials import *
from print_params import *
from makeRVE import *
from makecolumn import *
from acoustictest import *
from cyclemanager import *
from plotting import *
from functional import *
import time

def pipelineRun(O, RVE, MAT, TEST, OPT, AUX, pl, dft, ufft):

    if (OPT['verbose']):
        print (clr.END + clr.BOLD + clr.VIOLET + "--------------------------------------------------------------------------------")
        print (clr.END + clr.BOLD + clr.VIOLET + "                 ENTERING ACOUSTIC DEM PIPELINE                      ")
        print (clr.END + clr.BOLD + clr.VIOLET + "--------------------------------------------------------------------------------")

    # Initialize Omega
    O.load(OPT['zeropath'] + 'zero.yade.gz')

    # Print input parameters
    if (OPT['verbose']): printParams(RVE, MAT, TEST, OPT)

    # Create RVE geometry
    O = makeRVE(O, RVE, MAT, OPT, pl)

    # Create column out of RVE
    O = makeColumn(O, OPT)

    # Running the acoustic test
    O = acousticTest(O, TEST, OPT, AUX, pl, dft, ufft)

    # Plotting
    O = plotGeometry(OPT, pl)

    time.sleep(2) # async. computations

    # Toy functional to minimize

    fun = functional(RVE, OPT, AUX)

    time.sleep(1)

    if (OPT['verbose']):
        print (clr.END + clr.BOLD + clr.VIOLET + "--------------------------------------------------------------------------------")
        print (clr.END + clr.BOLD + clr.VIOLET + "                 PIPELINE RUN DONE                      ")
        print (clr.END + clr.BOLD + clr.VIOLET + "--------------------------------------------------------------------------------")

    return fun



