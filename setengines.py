### make setengines.py

# This file describes all the engines used in the framework
import numpy as np
from yade.utils import *
import os.path
from color_lib import clr

def addPlotData(triax, OPT):
   if (OPT['verbose']): print( clr.END + clr.BOLD + clr.BLUE,
                              "stress: ",
                              clr.END + clr.BOLD + clr.WHITE,"("
                              '{0:10.4e}'.format(triax.stress[0]),
                              ",", '{0:10.4e}'.format(triax.stress[1]),
                              ",", '{0:10.4e}'.format(triax.stress[2]),") Pa",
                              clr.END + clr.BOLD + clr.VIOLET,
                              "\n strain: ",
                              clr.END + clr.BOLD + clr.WHITE,"("
                              '{0:10.4f}'.format(triax.strain[0]),
                              ",", '{0:10.4f}'.format(triax.strain[1]),
                              ",", '{0:10.4f}'.format(triax.strain[2]),")")

def compactionFinished(O, triax, RVE, OPT):
    O.cell.trsf=Matrix3.Identity
    triax.goal=(RVE['sigmaIso'],RVE['sigmaIso'],RVE['sigmaIso'])
    triax.stressMask=7
    triax.maxStrainRate=(1.,1.,1.)
    triax.doneHook=' '
    triax.maxUnbalanced=10
    triax.dead=True
    triaxFinished(O, OPT)
    return O


def triaxFinished(O, OPT):
    if (OPT['verbose']):
        print(clr.END + clr.BOLD + clr.BLUE + "--------------------------------------------------------------------------------")
        print(clr.END + clr.BOLD + clr.BLUE + "Triaxial compression finished")
    saveState(O, OPT)
    return O


def saveState(O, OPT):
    if (OPT['saveStates']):
        path = os.getcwd() + OPT['wdir']
        if ( not os.path.exists( path )):
            os.mkdir( path )
        path = os.getcwd() + OPT['wdir'] + OPT['nrun'] + '/'
        if ( not os.path.exists( path )):
            os.mkdir( path )
        O.save( path + OPT['nrun'] + '_rve.yade.gz')
        if (OPT['verbose']): print(clr.END + clr.BOLD + clr.YELLOW, "RVE state saved")
    O.pause();


def set_engines_peri(RVE, MAT, O): # Engines for a periodic triaxial test
    tol = 1e-6
    unbalForc = 1e-5
    rate=(1e-4, 1e-4, 1e-4)
    if (MAT['model_type']=='Linear'):
        O.engines = [
            ForceResetter(),
            InsertionSortCollider([Bo1_Sphere_Aabb()],allowBiggerThanPeriod=True),
            InteractionLoop([Ig2_Sphere_Sphere_ScGeom6D()],
                            [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(
                                setCohesionNow=True,
                                setCohesionOnNewContacts=False)
                            ],
                            [Law2_ScGeom6D_CohFrictPhys_CohesionMoment(),
                             ]
                            ),
            PeriTriaxController(label='triax',
                                # specify target values and whether they are strains or stresses
                                goal=(RVE['sigmaIso'], RVE['sigmaIso'], RVE['sigmaIso']), stressMask=7,
                                # type of servo-control
                                dynCell=True, maxStrainRate=rate,
                                # wait until the unbalanced force goes below this value
                                maxUnbalanced=unbalForc, relStressTol=tol,
                                # call this function when goal is reached and the packing is stable
                                doneHook='compactionFinished(O, triax, RVE, OPT)'
                                ),
            NewtonIntegrator(damping=RVE['damping']),
            PyRunner(command='addPlotData(triax, OPT)', iterPeriod=100, label = 'addPlotData'),
        ]
    if (MAT['model_type']=='Hertz'):
        O.engines = [
            ForceResetter(),
            InsertionSortCollider([Bo1_Sphere_Aabb()]),
            InteractionLoop(
                [Ig2_Sphere_Sphere_ScGeom()],
                [Ip2_FrictMat_FrictMat_MindlinPhys()],
                [Law2_ScGeom_MindlinPhys_Mindlin()]
            ),
            PeriTriaxController(label='triax',
                                # specify target values and whether they are strains or stresses
                                goal=(RVE['sigmaIso'], RVE['sigmaIso'], RVE['sigmaIso']), stressMask=7,
                                # type of servo-control
                                dynCell=True, maxStrainRate=rate,
                                # wait until the unbalanced force goes below this value
                                maxUnbalanced=unbalForc, relStressTol=tol,
                                # call this function when goal is reached and the packing is stable
                                doneHook='compactionFinished(O, triax, RVE, OPT)'
                                ),
            NewtonIntegrator(damping=RVE['damping']),
            PyRunner(command='addPlotData(triax, OPT)', iterPeriod=100, label = 'addPlotData'),
        ]
    return O


def prefinished(O):
   triax.doneHook='finished(O)'
   print("Done")
   O.pause()
   return O

def finished(O):
    triax.dead=True
    print("Done")
    O.pause()
    return O
