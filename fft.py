### fft.py

# We encapsulate all DFT stuff into a convenient class
from yade.utils import aabbExtrema
import numpy as np
import multiprocessing
import os.path
from scipy.optimize import curve_fit
import time
from color_lib import clr

class dftSolver(object):

        def __init__(self, O, TEST, OPT):
                dMean = 1
                self.L = 10
                self._L = 0.1
                self.NKR = int(round(self.L / dMean))
                self.dMean = float(self.L / self.NKR)
                self.ISTEP = TEST['dftEvery']
                self.DT = TEST['EX_TS']
                self.OSTEP = int(TEST['propagationSteps'] / self.ISTEP)
                self.dkr = 2.0 * np.pi * self._L
                self.nkx = 0
                self.nky = 0
                self.nkz = 1
                self.z_beg = 0
                self.z_end = self.L
                self.dir = OPT['wdir']
                self.run = OPT['nrun']
                self.splitVels = np.zeros([self.NKR, 18, self.OSTEP])
                self.verbose = OPT['verbose']
                self.pVelocity = 0
                self.sVelocity = 0
                self.waveType = TEST['waveType']

                if (self.verbose): print(clr.END + clr.BOLD + clr.RED + "DFT solver object created ")



        def reinit(self, O, TEST, OPT, RVE):
                from yade.utils import aabbExtrema
                min, max = aabbExtrema(0.0, True)
                #dMean = (max[2] - min[2])/(2 * RVE['props']['N_z'] - 1.0) # FFT spatial step (strain accounted)
                dMean = 2. * np.mean([b.shape.radius for b in O.bodies])
                #print("dMean_=", dMean)
                if OPT['dftMode']=='all':
                    self.z_beg = 0
                    self.z_end = 2 * RVE['props']['N_z']
                if OPT['dftMode']=='detector':
                    self.z_beg = 2 * (1 + RVE['props']['N_b']+RVE['props']['N_c'])
                    self.z_end = 2 * RVE['props']['N_d']

                #print("resulting z_beg, z_end:", self.z_beg, self.z_end)
                self.L = dMean * (self.z_end - self.z_beg)
                #print("L=",self.L)

                self._L = 1. / self.L
                self.NKR = int(round(self.L / dMean))

                #print("NKR=",self.NKR)

                self.dMean = float(self.L / self.NKR)
                #print("dMean=", self.dMean)
                self.ISTEP = TEST['dftEvery']
                self.DT = TEST['EX_TS']
                self.OSTEP = int((TEST['propagationSteps'] - TEST['startFFTStep']) / self.ISTEP)
                self.dkr = 2.0 * np.pi * self._L
                self.nkx = 0
                self.nky = 0
                self.nkz = 1
                self.dir = OPT['wdir']
                self.run = OPT['nrun']
                self.splitVels = np.zeros([self.NKR, 18, self.OSTEP])
                self.pVelocity = 0
                self.sVelocity = 0
                self.startIter = TEST['startFFTStep']
                self.waveType = TEST['waveType']
                if (self.verbose): print(clr.END + clr.BOLD + clr.RED + "DFT solver class initialized:", self.splitVels.shape )



        def on_the_fly(self, O):
                if (self.verbose): print(clr.END + clr.BOLD + clr.RED + "Checkpoint DFT computations:", (O.iter - self.startIter) // self.ISTEP - 1 )
                self.splitVels[:, :, (O.iter - self.startIter) // self.ISTEP - 0] = np.array(self.curren((O.iter - self.startIter) // self.ISTEP - 1, O))

        def final_dft(self, AUX):
                # Power-spectra
                psl, pst, pso, DOM = self.spectr(self.OSTEP, self.DT, self.NKR, self.ISTEP)
                # frequency and wave number axes
                extent = np.array([self.dkr, self.NKR * self.dkr, -self.OSTEP / 2.0 * DOM / 1.0, self.OSTEP / 2.0 * DOM / 1.0]) / (2. * np.pi)
                # save spectra and ranges of frequency and wave number

                path = os.getcwd() + self.dir
                if ( not os.path.exists( path )):
                    os.mkdir( path )

                path = path + self.run + '/'
                if ( not os.path.exists( path )):
                    os.mkdir( path )

                path = path + 'dft/'
                if ( not os.path.exists( path )):
                    os.mkdir( path )

                np.save(path + "densities.npy", (self.splitVels, psl, pst, pso, extent))
                if (self.verbose): print(clr.END + clr.BOLD + clr.RED + "Final DFT computations")

                # Compute P/S-wave velocity from spectral data
                func = lambda x, a, b, c: a/((x-b)**2+c**2)  # a = s * c
                if self.waveType=='P':
                    fitData = psl
                if self.waveType=='S':
                    fitData = pst


                N = np.shape(fitData)[0]
                NOR = np.sum(fitData[:,:])
                nn = N//5 # Five frequency bands
                print("dft band width:", nn)
                bands = np.zeros([5])
                bands[0] = np.sum(fitData[0:nn,:]) / NOR
                bands[1] = np.sum(fitData[nn:2*nn,:]) / NOR
                bands[2] = np.sum(fitData[2*nn:3*nn,:]) / NOR
                bands[3] = np.sum(fitData[3*nn:4*nn,:]) / NOR
                bands[4] = np.sum(fitData[4*nn:5*nn,:]) / NOR

                return bands




        # Some legacy functions - need to be remastered

        def curren(self, ostep, O):
                from itertools import repeat
                """ function to calculate Fourier transforms of longitudinal/transverse 
		                currents and angular velocities at time t (inside the time loop)
		                Params:
		                vx: particle velocity along x 
		                vy: particle velocity along y
		                vz: particle velocity along z
		                nkx: x-component of unit vector perpendicular to the wavenumber vector
		                nky: y-component of unit vector perpendicular to the wavenumber vector
		                nkz: z-component of unit vector parallel to the wavenumber vector
		                ostep: current tim estep (ostep = 0,1,...OSTEP-1)
	            """
                # global variable unit vector of wavenumber, sample length and number of particles
                #~ print "Processing data at NO. %i step..."%(ostep)
                # get particles that contribute to state state
                bodies = [b for b in O.bodies if len(b.intrs())>1 and b.state.pos[2] > self.z_beg and b.state.pos[2] < self.z_end]
                # shear direction along x and y; compression direction along z
                x = np.array([b.state.pos[0] for b in bodies])
                y = np.array([b.state.pos[1] for b in bodies])
                z = np.array([b.state.pos[2] for b in bodies])
                vx = np.array([b.state.vel[0] for b in bodies])
                vy = np.array([b.state.vel[1] for b in bodies])
                vz = np.array([b.state.vel[2] for b in bodies])
                ox = np.array([b.state.angVel[0] for b in bodies])
                oy = np.array([b.state.angVel[1] for b in bodies])
                oz = np.array([b.state.angVel[2] for b in bodies])
                with multiprocessing.Pool(1) as pool:
                    # Longitudinal, transverse, and rotational velocities (real&imag) at ostep
                    params = zip(range(self.NKR),repeat([x,y,z,vx,vy,vz,ox,oy,oz]))
                    splitVels = np.array(pool.map(self.split,params))
                time.sleep(1)
                pool.close()
                pool.terminate()
                return splitVels

        def split(self, params):
                j, params = params
                x, y, z, vx, vy, vz, ox, oy, oz = params
                # Now the mass is assumed to be same. Modify below if the mass of particles is different
                vcx = np.mean(vx);
                vcy = np.mean(vy);
                vcz = np.mean(vz);
                # 1) Initialization
                # The real parts of x-, y- and z-components of Fourier transform of particle velocities.
                Re_ukx = 0.0;
                Re_uky = 0.0;
                Re_ukz = 0.0;
                # The imaginary parts of x-, y- and z-components of Fourier transform of particle velocities.
                Im_ukx = 0.0;
                Im_uky = 0.0;
                Im_ukz = 0.0;
                # 2) Wave number vector
                # kr is the length of wavenumber vector and kx and ky are the x-, y- and z-components of wave number vector.
                kr = (j + 1) * self.dkr;
                kx = kr * self.nkx;
                ky = kr * self.nky;
                kz = kr * self.nkz;
                # This inner loop (now use element-wise operation) calculates the Fourier transforms of particle velocity and angular velocity.
                # 3) Phase
                # LH is a half of system size, L (i.e. LH=L/2). Here, the center of simulation box is moved to the origin (we may not need this procedure)
                LH = self.L / 2.
                # This is the inner product of wave number vector and particle position, −k*r_i(t), check Kuniyasu's slide.
                bs = -(kx * (x - LH) + ky * (y - LH) + kz * (z - LH));
                # 4) each current (subtracted by the center-of-mass velocity)
                # These are the outputs of this inner loop (element-wise operation of numpy).
                # Fourier transform of particle linear velocity
                Re_ukx = np.sum((vx - vcx) * np.cos(bs));
                Re_uky = np.sum((vy - vcy) * np.cos(bs));
                Re_ukz = np.sum((vz - vcz) * np.cos(bs));
                Im_ukx = np.sum((vx - vcx) * np.sin(bs));
                Im_uky = np.sum((vy - vcy) * np.sin(bs));
                Im_ukz = np.sum((vz - vcz) * np.sin(bs));
                # Fourier transform of particle angular velocity
                Re_omx = np.sum(ox * np.cos(bs));
                Re_omy = np.sum(oy * np.cos(bs));
                Re_omz = np.sum(oz * np.cos(bs));
                Im_omx = np.sum(ox * np.sin(bs));
                Im_omy = np.sum(oy * np.sin(bs));
                Im_omz = np.sum(oz * np.sin(bs));
                # 5) longitudinal current
                # The real parts of x-, y- and z-components of Fourier transforms of longitudinal current, i.e. Re[u_k^{||}(t)] = Re[u_k(t)]*n
                Re_ulx = (self.nkx * Re_ukx + self.nky * Re_uky + self.nkz * Re_ukz) * self.nkx;
                Re_uly = (self.nkx * Re_ukx + self.nky * Re_uky + self.nkz * Re_ukz) * self.nky;
                Re_ulz = (self.nkx * Re_ukx + self.nky * Re_uky + self.nkz * Re_ukz) * self.nkz;
                # The imaginary parts of x-, y- and z-components of Fourier transforms of longitudinal current, i.e. Im[u_k^{||}(t)] = Im[u_k(t)]*n
                Im_ulx = (self.nkx * Im_ukx + self.nky * Im_uky + self.nkz * Im_ukz) * self.nkx;
                Im_uly = (self.nkx * Im_ukx + self.nky * Im_uky + self.nkz * Im_ukz) * self.nky;
                Im_ulz = (self.nkx * Im_ukx + self.nky * Im_uky + self.nkz * Im_ukz) * self.nkz;
                # 6) transverse current
                # The real parts of x-, y- and z-components of Fourier transforms of transverse current, i.e. Re[u_k^{|_}(t)] = Re[u_k(t)]-Re[u_k^{||}(t)]
                Re_utx = Re_ukx - Re_ulx;
                Re_uty = Re_uky - Re_uly;
                Re_utz = Re_ukz - Re_ulz;
                # The imaginary parts of x-, y- and z-components of Fourier transforms of transverse current, i.e. Im[u_k^{|_}(t)] = Im[u_k(t)]-Im[u_k^{||}(t)]
                Im_utx = Im_ukx - Im_ulx;
                Im_uty = Im_uky - Im_uly;
                Im_utz = Im_ukz - Im_ulz;
                return Re_ulx, Re_uly, Re_ulz, Re_utx, Re_uty, Re_utz, Im_ulx, Im_uly, Im_ulz, Im_utx, Im_uty, Im_utz, \
                       Re_omx, Re_omy, Re_omz, Im_omx, Im_omy, Im_omz

        def spectr(self, OSTEP, DT, NKR, ISTEP):
                from itertools import product, repeat
                # save simulation data per ISTEP calculation cycle
                DTT = DT * ISTEP;
                # lowest angular frequency omega = 2*pi/T
                DOM = 2.0 * np.pi / (OSTEP * DTT);
                # use a multiprocessing pool to parallelize the code
                with multiprocessing.Pool(1) as pool:
                    nkr = range(NKR);
                    ostep = range(OSTEP + 1)
                    paramlist = zip(list(product(nkr, ostep)), repeat([DTT, OSTEP, DOM]))
                    pslPstAndPsoLists = pool.map(self.DFT, paramlist)
                time.sleep(1)
                pool.close()
                pool.terminate()
                pslPstAndPsoLists = np.array(pslPstAndPsoLists)
                psl = pslPstAndPsoLists[:, 0].reshape(NKR, OSTEP + 1)
                pst = pslPstAndPsoLists[:, 1].reshape(NKR, OSTEP + 1)
                pso = pslPstAndPsoLists[:, 2].reshape(NKR, OSTEP + 1)
                return psl, pst, pso, DOM

        def DFT(self, paramlist):
                # get decomposed velocities
                Re_ulx = self.splitVels[:, 0, :];
                Re_uly = self.splitVels[:, 1, :];
                Re_ulz = self.splitVels[:, 2, :];
                Re_utx = self.splitVels[:, 3, :];
                Re_uty = self.splitVels[:, 4, :];
                Re_utz = self.splitVels[:, 5, :];
                Im_ulx = self.splitVels[:, 6, :];
                Im_uly = self.splitVels[:, 7, :];
                Im_ulz = self.splitVels[:, 8, :];
                Im_utx = self.splitVels[:, 9, :];
                Im_uty = self.splitVels[:, 10, :];
                Im_utz = self.splitVels[:, 11, :];
                Re_omx = self.splitVels[:, 12, :];
                Re_omy = self.splitVels[:, 13, :];
                Re_omz = self.splitVels[:, 14, :];
                Im_omx = self.splitVels[:, 15, :];
                Im_omy = self.splitVels[:, 16, :];
                Im_omz = self.splitVels[:, 17, :];
                # get the parameters
                i, j = paramlist[0]
                DTT, OSTEP, DOM = paramlist[1]
                om = (j - OSTEP / 2.0) * DOM;
                # Initialization
                Re_slx = 0.0;
                Re_sly = 0.0;
                Re_slz = 0.0;
                Re_stx = 0.0;
                Re_sty = 0.0;
                Re_stz = 0.0;
                Re_sox = 0.0;
                Re_soy = 0.0;
                Re_soz = 0.0;
                Im_slx = 0.0;
                Im_sly = 0.0;
                Im_slz = 0.0;
                Im_stx = 0.0;
                Im_sty = 0.0;
                Im_stz = 0.0;
                Im_sox = 0.0;
                Im_soy = 0.0;
                Im_soz = 0.0;
                """ what is the code below doing? """
                for k in range(OSTEP):
                        tt = k * DTT;
                        # Fourier transform in time
                        cosForFT = np.cos(om * tt);
                        sinForFT = np.sin(om * tt)
                        Re_slx += (Re_ulx[i, k] * cosForFT - Im_ulx[i, k] * sinForFT) * DTT;
                        Re_sly += (Re_uly[i, k] * cosForFT - Im_uly[i, k] * sinForFT) * DTT;
                        Re_slz += (Re_ulz[i, k] * cosForFT - Im_ulz[i, k] * sinForFT) * DTT;
                        Re_stx += (Re_utx[i, k] * cosForFT - Im_utx[i, k] * sinForFT) * DTT;
                        Re_sty += (Re_uty[i, k] * cosForFT - Im_uty[i, k] * sinForFT) * DTT;
                        Re_stz += (Re_utz[i, k] * cosForFT - Im_utz[i, k] * sinForFT) * DTT;
                        Re_sox += (Re_omx[i, k] * cosForFT - Im_omx[i, k] * sinForFT) * DTT;
                        Re_soy += (Re_omy[i, k] * cosForFT - Im_omy[i, k] * sinForFT) * DTT;
                        Re_soz += (Re_omz[i, k] * cosForFT - Im_omz[i, k] * sinForFT) * DTT;
                        Im_slx += (Im_ulx[i, k] * cosForFT + Re_ulx[i, k] * sinForFT) * DTT;
                        Im_sly += (Im_uly[i, k] * cosForFT + Re_uly[i, k] * sinForFT) * DTT;
                        Im_slz += (Im_ulz[i, k] * cosForFT + Re_ulz[i, k] * sinForFT) * DTT;
                        Im_stx += (Im_utx[i, k] * cosForFT + Re_utx[i, k] * sinForFT) * DTT;
                        Im_sty += (Im_uty[i, k] * cosForFT + Re_uty[i, k] * sinForFT) * DTT;
                        Im_stz += (Im_utz[i, k] * cosForFT + Re_utz[i, k] * sinForFT) * DTT;
                        Im_sox += (Im_omx[i, k] * cosForFT + Re_omx[i, k] * sinForFT) * DTT;
                        Im_soy += (Im_omy[i, k] * cosForFT + Re_omy[i, k] * sinForFT) * DTT;
                        Im_soz += (Im_omz[i, k] * cosForFT + Re_omz[i, k] * sinForFT) * DTT;
                # Power-spectra
                psl_ij = Re_slx * Re_slx + Im_slx * Im_slx \
                         + Re_sly * Re_sly + Im_sly * Im_sly \
                         + Re_slz * Re_slz + Im_slz * Im_slz;
                pst_ij = Re_stx * Re_stx + Im_stx * Im_stx \
                         + Re_sty * Re_sty + Im_sty * Im_sty \
                         + Re_stz * Re_stz + Im_stz * Im_stz;
                pso_ij = Re_sox * Re_sox + Im_sox * Im_sox \
                         + Re_soy * Re_soy + Im_soy * Im_soy \
                         + Re_soz * Re_soz + Im_soz * Im_soz;
                return psl_ij, pst_ij, pso_ij

