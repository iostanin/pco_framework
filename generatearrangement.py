### generatearrangement.py

# This file contains functions for creation of different kinds of RVEs
# for wave propagation experiments.
# These functions create initial (loose) arrangements that are then
# brought to a desirable stress state

import numpy as np
from makeparticle import makeParticle

# This function creates a phononic crystal made of layers of FCC arrangements
def generateFCCPhonon(O, N_pack, subcell_size, reg_type, margin, MAT):
    cc = subcell_size * (1 + margin)
    reg_rad = 0.25 * np.sqrt(2) * subcell_size
    Nx = N_pack[0]
    Ny = N_pack[1]
    Nz = N_pack[2]
    Nb = N_pack[3]
    Nc = N_pack[4]
    Nd = N_pack[5]
    #print("Nz=",Nz,"Nb=",Nb, "Nc=",Nc, "Nd=", Nd)
    beg = np.zeros(Nc)
    end = np.zeros(Nc)
    offset = 0.01   # Ensures that layers won't jump across periodic boundaries during compression
    for ic in range(Nc):
        beg[ic] = cc * (Nb + ic + 1)
        end[ic] = cc * (Nb + ic + 2)
    #print("begs:", beg)
    #print("ends:", end)


    for i in range(Nx):
        for ii in range(Ny):
            for iii in range(Nz):
                x = cc * (i   + offset)
                y = cc * (ii  + offset)
                z = cc * (iii + offset)
                center1 = [ x ,           y ,           z ]
                center2 = [ x + 0.5 * cc, y + 0.5 * cc, z ]
                center3 = [ x + 0.5 * cc, y,            z + 0.5 * cc]
                center4 = [ x,            y + 0.5 * cc, z + 0.5 * cc]
                kind = 0
                for ic in range(Nc):
                    if (z>beg[ic]) and (z<=end[ic]): kind = ic + 1
                if (z <=            cc): kind = Nc
                if (z >  (Nz - 1) * cc): kind = Nc
                makeParticle(kind, center1, reg_rad,  O,  MAT)
                makeParticle(kind, center2, reg_rad,  O,  MAT)
                makeParticle(kind, center3, reg_rad,  O,  MAT)
                makeParticle(kind, center4, reg_rad,  O,  MAT)
    return O

# This function creates a phononic crystal made of random clusters
def generateFCCRandom(O, N_pack, subcell_size, reg_type, margin, MAT, ParType):
    cc = subcell_size * (1 + margin)
    reg_rad = 0.25 * np.sqrt(2) * cc
    Nz = N_pack[2]
    Nb = N_pack[3]
    Nc = N_pack[4]
    Nd = N_pack[5]
    offset = 0.01
    beg = cc * (1 + Nb)
    end = cc * (1 + Nb + Nc)
    counter = 0
    for i in range(N_pack[0]):
        for ii in range(N_pack[1]):
            for iii in range(N_pack[2]):
                x = cc * (i   + offset)
                y = cc * (ii  + offset)
                z = cc * (iii + offset)
                center1 = [ x ,           y ,           z ]
                center2 = [ x + 0.5 * cc, y + 0.5 * cc, z ]
                center3 = [ x + 0.5 * cc, y,            z + 0.5 * cc]
                center4 = [ x,            y + 0.5 * cc, z + 0.5 * cc]
                kind1 = 0; kind2 = 0; kind3 = 0; kind4 = 0
                if (z > beg) and (z <= end):
                    kind1 = int(ParType[4 * counter    ])
                    kind2 = int(ParType[4 * counter + 1])
                    kind3 = int(ParType[4 * counter + 2])
                    kind4 = int(ParType[4 * counter + 3])
                    counter = counter + 1
                if (z < cc):            kind1 = Nc; kind2 = Nc; kind3 = Nc; kind4 = Nc
                if (z > (Nz - 1) * cc): kind1 = Nc; kind2 = Nc; kind3 = Nc; kind4 = Nc
                makeParticle(kind1, center1, reg_rad,  O,  MAT)
                makeParticle(kind2, center2, reg_rad,  O,  MAT)
                makeParticle(kind3, center3, reg_rad,  O,  MAT)
                makeParticle(kind4, center4, reg_rad,  O,  MAT)
    return O




