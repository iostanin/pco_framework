### makecolumn.py

# This script creates a column out of RVE created at the previous step

import numpy as np
from setengines import *
from yade import Vector3
import os.path
from waveaux import *
from cyclemanager import *
from u_fft import ufftSolver
from fft import dftSolver

from color_lib import clr

def acousticTest(O, TEST, OPT, AUX, pl, dft, ufft):

    O.resetTime()
    AUX['stage'] = 'Relaxation'

    # This runs the first stage of the cycle manager
    O.engines[-1].command = 'AUX = cycleManager(O, RVE, TEST, OPT, AUX, dft, ufft)'
    O.engines[-1].iterPeriod = 1
    O.engines[-1].dead = False
    if OPT['verbose']: print(clr.END + clr.BOLD + clr.GREEN, "Running the acoustic test")
    O.run(TEST['relaxationSteps']+1, True)


    return O

