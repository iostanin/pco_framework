### makecolumn.py

# This script creates a column out of RVE created at the prev step

import numpy as np
import os.path
from color_lib import clr
from yade import Matrix3

def makeColumn(O, OPT):
    from yade.utils import aabbExtrema
    if (OPT['verbose']): print(clr.END + clr.BOLD + clr.GREEN, "Fix RVE sizes, set grip layers")

    # Deactiate periTriax
    O.engines[5].dynCell = False
    extra = 0 # Expansion of the box in the direction of the wave propagation
    dSizeX,dSizeY,dSizeZ = O.cell.size
    O.cell.hSize = Matrix3(dSizeX,0,0, 0,dSizeY,0, 0,0,dSizeZ+extra)
    O.engines[5].dead = True

    # Save column
    O.saveTmp()
    path = os.getcwd() + OPT['wdir']
    if ( not os.path.exists( path )):
        os.mkdir( path )
    path = os.getcwd() + OPT['wdir'] + OPT['nrun'] + '/'
    if ( not os.path.exists( path )):
        os.mkdir( path )
    if (OPT['saveStates']):
        O.save( path + OPT['nrun'] + '_column.yade.gz')
        if (OPT['verbose']): print(clr.END + clr.BOLD + clr.YELLOW, "Column state saved")
    O.pause();
    if (OPT['verbose']): print(clr.END + clr.BOLD + clr.GREEN, "Column created successfully")
    return O

