### printing.py

# Output of the simulation parameters

from color_lib import clr
from generatearrangement import *
from creatematerials import *

def printParams(RVE, MAT, TEST, OPT):
        print("")
        print(clr.END + clr.BOLD + clr.YELLOW + "The pipeline is launched with the following list of options:" + clr.END)

        print(clr.END + clr.BOLD + clr.WHITE + "--------------------------------------------------------------------------------")
        print(clr.END + clr.BOLD + clr.GREEN + "RVE parameters:\n")
        print(clr.END + clr.BOLD + clr.WHITE, "Goal isotropic stress:    \t\t", '{0:10.2e}'.format(RVE['sigmaIso']), " Pa")
        print(clr.END + clr.BOLD + clr.WHITE, "Damping (assembly stage): \t\t", RVE['damping'])
        print(clr.END + clr.BOLD + clr.WHITE, "Timestep(assembly stage): \t\t", RVE['RVE_TS'], " s")
        print(clr.END + clr.BOLD + clr.WHITE, "RVE packing type:         \t\t", RVE['type'])
        print(clr.END + clr.BOLD + clr.WHITE, "\n","N_x:         \t\t", RVE['props']['N_x'], clr.END +  "\t - width of the column (cell periods) ")
        print(clr.END + clr.BOLD + clr.WHITE, "N_y:         \t\t", RVE['props']['N_y'], clr.END + "\t - thickness of the column (cell periods)")
        print(clr.END + clr.BOLD + clr.WHITE, "N_z:         \t\t", RVE['props']['N_z'], clr.END + "\t - length of the the column (cell periods)")

        print(clr.END + clr.BOLD + clr.WHITE, "N_b:         \t\t", RVE['props']['N_b'], clr.END + "\t - buffer layers (cell periods)")
        print(clr.END + clr.BOLD + clr.WHITE, "N_c:         \t\t", RVE['props']['N_c'], clr.END + "\t - phononic crystal layers (cell periods)")
        print(clr.END + clr.BOLD + clr.WHITE, "N_d:         \t\t", RVE['props']['N_d'], clr.END + "\t - detector layers (cell periods)")



        print("\n")

        print(clr.END + clr.BOLD + clr.GREEN + "Materials:\n")

        short = False
        if (short):
            for k in range(1):
                print(clr.END + clr.BOLD + clr.GREEN, MAT['mat_array'][k]['label'],": \t\t")
                print(clr.END + clr.BOLD + clr.WHITE, "  Young's modulus: ", '{0:10.2e}'.format(MAT['mat_array'][k]['young']), " Pa")
                print(clr.END + clr.BOLD + clr.WHITE, "  Poisson's ratio: ", '{0:10.2f}'.format(MAT['mat_array'][k]['poiss']))
                print(clr.END + clr.BOLD + clr.WHITE, "  Friction coeff.: ", '{0:10.2f}'.format(MAT['mat_array'][k]['frict']))
                print(clr.END + clr.BOLD + clr.WHITE, "  Density        : ", '{0:10.2e}'.format(MAT['mat_array'][k]['dens' ]), " kg/m^3")

                print(clr.END + clr.BOLD + clr.GREEN, "\n and its",len(MAT['mat_array']),"instances\n")
        else:
            for k in range(len(MAT['mat_array'])):
                print(clr.END + clr.BOLD + clr.GREEN, MAT['mat_array'][k]['label'],": \t\t")
                print(clr.END + clr.BOLD + clr.WHITE, "  Young's modulus: ", '{0:10.2e}'.format(MAT['mat_array'][k]['young']), " Pa")
                print(clr.END + clr.BOLD + clr.WHITE, "  Poisson's ratio: ", '{0:10.2f}'.format(MAT['mat_array'][k]['poiss']))
                print(clr.END + clr.BOLD + clr.WHITE, "  Friction coeff.: ", '{0:10.2f}'.format(MAT['mat_array'][k]['frict']))
                print(clr.END + clr.BOLD + clr.WHITE, "  Density        : ", '{0:10.2e}'.format(MAT['mat_array'][k]['dens' ]), " kg/m^3")
                print("\n\n")

        print(clr.END + clr.BOLD + clr.GREEN + "Test options:\n")

        print(clr.END + clr.BOLD + clr.WHITE, "Number of relaxation timesteps   : \t\t", TEST['relaxationSteps'])
        print(clr.END + clr.BOLD + clr.WHITE, "Number of agitation timesteps    : \t\t", TEST['agitationSteps'])
        print(clr.END + clr.BOLD + clr.WHITE, "Number of propagation timesteps  : \t\t", TEST['propagationSteps'])
        print(clr.END + clr.BOLD + clr.WHITE, "Print checkpoint every           : \t\t", TEST['printEvery'], "cycles")
        print(clr.END + clr.BOLD + clr.WHITE, "Save checkpoint every            : \t\t", TEST['saveEvery'], "cycles")
        print(clr.END + clr.BOLD + clr.WHITE, "DFT checkpoint every             : \t\t", TEST['dftEvery'], "cycles")
        print(clr.END + clr.BOLD + clr.WHITE, "Pre-relaxation timestep          : \t\t", TEST['REL_TS'], " s")
        print(clr.END + clr.BOLD + clr.WHITE, "Test timestep                    : \t\t", TEST['EX_TS'], " s")
        print(clr.END + clr.BOLD + clr.WHITE, "Wave shape (sin, cos, lin)       : \t\t", TEST['waveShape'])
        print(clr.END + clr.BOLD + clr.WHITE, "Wave type (normal or shear)      : \t\t", TEST['waveShape'])
        print(clr.END + clr.BOLD + clr.WHITE, "Wave  magnitude                  : \t\t", TEST['waveMag'], " m")



        print("\n")

        print(clr.END + clr.BOLD + clr.GREEN + "Additional options:\n")
        for k, v in OPT.items():
            print(clr.END + clr.BOLD + clr.WHITE, k,": \t\t\t", v)
        print(clr.END + clr.BOLD + clr.WHITE+"--------------------------------------------------------------------------------")
        print("\n")
