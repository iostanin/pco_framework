# This script plots the velocity profile observed in the wave propagation simulation
# usage: python3 profile.py [-b N] [-o M]
# When run without parameters it plots the resutls of a single run simulation
# When running with single number parameter (e.g python3 profile.py 3), it plots
# the corresponding profile from batch series
# When running with -b or -o options this script will plot the corresponding
# profiles from either batch or optimizaiton folder
#------------------------------------------------------------------------------------

import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

params = {'lines.linewidth': 1.5,
          'backend': 'ps',
          'axes.labelsize': 18,
          'axes.titlesize': 18,
          'font.size': 18,
          'legend.fontsize': 13,
          'xtick.labelsize': 15,
          'ytick.labelsize': 15,
          'text.usetex': True,
          'font.family': 'serif',
          'legend.edgecolor': 'k',
          'legend.fancybox':False}

matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams.update(params)

def loadData():

    # Define the paths based on input arguments
    if (len(sys.argv)==1):                            # no arguments - single run
        path = './singlerun/profile'
        label = 'SINGLE RUN'
    if (len(sys.argv)==2):                            # single argument - batch run
        path = './batch/run_' + str(sys.argv[1]) + '/profile'
        label = 'BATCH RUN ' + str(sys.argv[1])
    if (len(sys.argv)==3):                            # Full format: run -b 2,  run -o 3
        if (sys.argv[1]=="-b"):
            path = './batch/run_' + str(sys.argv[2]) + '/profile'
            label = 'BATCH RUN ' + str(sys.argv[2])
        elif (sys.argv[1]=="-o"):
            path = './optimization/run_' + str(sys.argv[2]) + '/profile'
            label = 'OPTIMIZATION RUN ' + str(sys.argv[2])

    filename = ( path + '/info.npy')
    T, N = np.load(filename)
    filename = ( path + '/vel.npy')
    vel = np.load(filename)
    data = np.zeros([N,T])
    x = np.arange(0, N)
    for i in range(T):
        filename = ( path + '/profile' + str((i+1)*50)+'.npy')
        data[:,i] = np.load(filename)
    return data, x, T, N, vel, label

def init():  # only required for blitting to give a clean slate.
    line.set_ydata([np.nan] * len(x))
    return line,

def animate(i):
    line.set_ydata(data[:,i%T])  # update the data.
    return line,

#--------------------------Beginning of the script-------------------------------------
data, x, T, N, vel, label = loadData()
min, max = np.min(data), np.max(data)
fig, ax = plt.subplots(figsize=(10.0, 7.0))
plt.xlabel(r'Number of layer');
plt.ylabel(r'layer velocity');
plt.title(label, fontsize=30)
plt.text(int(0.72*N), 1.05 * max , r'$V_f^{Peak}=$ '+str(int(vel))+' m/s', fontsize=20)
print(vel)
line, = ax.plot(x, min + (max - min) * x/N) # This sets the plot scales
ani = animation.FuncAnimation(
    fig, animate, init_func=init, interval=100, blit=True, save_count=50)
plt.show()
