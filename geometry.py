# This script draws the final geometry obtained in the yade simulation
# usage: yade geometry.py [-b N] [-o M]
# When run without parameters it plots the resutls of a single run simulation
# When running with single number parameter (e.g python3 profile.py 3), it plots
# the corresponding geometry from batch series
# When running with -b or -o options this script will plot the corresponding
# geometries from either batch or optimizaiton folder
#------------------------------------------------------------------------------------

import getpass
import time
user = getpass.getuser()
sys.path.append(os.getcwd())
import numpy as np
from yade import qt, Vector3
from yade.utils import aabbExtrema
import os.path

# Define the paths based on input arguments
if (len(sys.argv)==1):                              # no arguments - single run
    path = './singlerun/_final.yade.gz'
if (len(sys.argv)==2):                            # single argument - batch run
    path = './batch/run_' + str(sys.argv[1]) + '/run_' + str(sys.argv[1])+ '_final.yade.gz'
if (len(sys.argv)==3):                       # Full format: run -b 2,  run -o 3
    if (sys.argv[1]=="-b"):
        path = './batch/run_' + str(sys.argv[2]) + '/run_' + str(sys.argv[2])+ '_final.yade.gz'
    elif (sys.argv[1]=="-o"):
        path = './optimization/run_' + str(sys.argv[2]) + '/run_' + str(sys.argv[2])+ '_final.yade.gz'
if os.path.exists(path):
        print("Loading RVE...")
        O.load(path);

mn,mx = aabbExtrema()
lookAt = 0.5 * (mx + mn)
rr = qt.Renderer()
vv = qt.View()
cc = qt.Controller()
rr.bgColor = Vector3(1., 1., 1.)
rr.light1 = True
rr.light2 = True
rr.ghosts = False
rr.light2Color = Vector3(0.2, 0., 0.)
vv.sceneRadius = 4 * lookAt[2]
vv.showEntireScene = True
vv.eyePosition = 1.2 * Vector3(lookAt[2], lookAt[2], 0)
vv.lookAt = lookAt
