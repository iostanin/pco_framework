# This script plots the wave spectrum observed in the wave propagation simulation
# usage: python3 spectrum.py [-b N] [-o M]
# When run without parameters it plots the resutls of a single run simulation
# When running with single number parameter (e.g python3 profile.py 3), it plots
# the corresponding spectrum from batch series
# When running with -b or -o options this script will plot the corresponding
# profiles from either batch or optimizaiton folder
#------------------------------------------------------------------------------------

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
#import glob
#import xlrd
import sys
#from scipy.optimize import curve_fit
import matplotlib
#from scipy.signal import *

print('Number of arguments:', len(sys.argv), 'arguments.')
print('Argument List:', str(sys.argv))

params = {'lines.linewidth': 1.5,
          'backend': 'ps',
          'axes.labelsize': 18,
          'axes.titlesize': 18,
          'font.size': 18,
          'legend.fontsize': 13,
          'xtick.labelsize': 15,
          'ytick.labelsize': 15,
          'text.usetex': False,
          'font.family': 'serif',
          'legend.edgecolor': 'k',
          'legend.fancybox':False}

#matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams.update(params)

# Define the paths based on input arguments
if (len(sys.argv)==1):                            # no arguments - single run
    path = './singlerun/dft/'
    label = 'SINGLE RUN'
if (len(sys.argv)==2):                            # single argument - batch run
    path = './batch/run_' + str(sys.argv[1]) + '/dft/'
    label = 'BATCH RUN ' + str(sys.argv[1])
if (len(sys.argv)==3):                            # Full format: run -b 2,  run -o 3
    if (sys.argv[1]=="-b"):
        path = './batch/run_' + str(sys.argv[2]) + '/dft/'
        label = 'BATCH RUN ' + str(sys.argv[2])
    elif (sys.argv[1]=="-o"):
        path = './optimization/run_' + str(sys.argv[2]) + '/dft/'
        label = 'OPTIMIZATION RUN ' + str(sys.argv[2])

data = np.load(path + 'densities.npy', allow_pickle=True)
splitVels, psl, pst, pso, extent = data
plt.figure(figsize=(10.0,7.0)); vAdd = 0
plt.title(label, fontsize=30)
ax = plt.imshow(psl.transpose(),extent=extent,cmap=cm.seismic,interpolation='bilinear',origin='lower',aspect='auto');

plt.show()

print(psl.shape)
