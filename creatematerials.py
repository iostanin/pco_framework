### creatematerials.py

# This function creates materials for the experiment
# We'll use absolute values for physical quantities
# For now we'll use the following materials:
# plastic (porous particles)
# glass (regular particles of the 1st kind)
# rubber (regular particles of the 2nd kind)


def createMaterials(MAT, O):
    from yade.wrapper import CohFrictMat
    from yade.wrapper import FrictMat
    import numpy as np
    mat_ids = []
    if (MAT['model_type']=='Linear'):
        for i in range (len(MAT['mat_array'])):
            id = O.materials.append(CohFrictMat(young=MAT['mat_array'][i]['young'],
                                                  poisson=MAT['mat_array'][i]['poiss'],
                                                  density=MAT['mat_array'][i]['dens'],
                                            frictionAngle=MAT['mat_array'][i]['frict'],
                                                    isCohesive=False,
                                                    normalCohesion=0,
                                                    shearCohesion=0,
                                                    etaRoll=-1,
                                                    momentRotationLaw=False,
                                                    label=MAT['mat_array'][i]['label']))


    if (MAT['model_type']=='Hertz'): 
        for i in range(len(MAT['mat_array'])):
            id = O.materials.append(CohFrictMat(young=MAT['mat_array'][i]['young'],
                                                  poisson=MAT['mat_array'][i]['poiss'],
                                                  density=MAT['mat_array'][i]['dens'],
                                            frictionAngle=MAT['mat_array'][i]['frict'],
                                                    isCohesive=False,
                                                    normalCohesion=0,
                                                    shearCohesion=0,
                                                    etaRoll=-1,
                                                    momentRotationLaw=False,
                                                    label=MAT['mat_array'][i]['label']))

    return O

