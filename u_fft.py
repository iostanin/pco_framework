### u_fft.py

# This class performs fft transform of space-time velocity data
# and reconstructs frequency-wavenumber distribution of energy,
# allowing to see the dispersion branches in the studied assembly

# The procedure uses standard fft from scipy library.
# It maintins only uniform sampling, therefore we work with
# the array of equispaced space-time slices of normal/shear velocity
# distribution. The velocity is averaged within every slice at every
# time checkpoint, perturbations of velocities of individual particles
# are not considered.

from yade.utils import aabbExtrema
import numpy as np
import os.path
from color_lib import clr
from scipy import fft, ifft # python 3.5 style
#from scipy.fft import fft, ifft # python 3.8 style


class ufftSolver(object):

        def __init__(self, OPT):
            self.verbose = OPT['verbose']
            if (self.verbose): print(clr.END + clr.BOLD + clr.BEIGE + "UFFT solver object created ")

        def reinit(self, O, TEST, OPT, RVE):
            self.dir = OPT['wdir']
            self.run = OPT['nrun']
            self.verbose = OPT['verbose']
            self.dt = TEST['EX_TS'] * TEST['dftEvery']
            self.N_t = 0
            if OPT['dftMode']=='all':
                self.beg = 0
                self.N_l = 2 * RVE['props']['N_z']
            if OPT['dftMode']=='detector':
                self.beg = 2 * (1 + RVE['props']['N_b']+RVE['props']['N_c'])
                self.N_l = 2 * RVE['props']['N_d']
            from yade.utils import aabbExtrema
            min, max = aabbExtrema(0.0, True)
            self.dl = (max[2] - min[2])/(2 * RVE['props']['N_z'] - 1.0) # FFT spatial step (strain accounted)
            self.space_time_array = np.zeros([self.N_l]) # Zero layer of space time array
            if (TEST['waveType']=='P'): self.index = 2
            if (TEST['waveType']=='S'): self.index = 0
            if (self.verbose): print(clr.END + clr.BOLD + clr.BEIGE + "UFFT solver object initialized")

        def on_the_fly(self, O):
            # averaging velocities
            layer = np.zeros([self.N_l])
            for l in range(self.N_l):
                k = 0 # number of particles in the layer
                for b in O.bodies:
                    if (b.state.pos[2] > -0.5*self.dl + self.beg*self.dl + l*self.dl and b.state.pos[2] <= -0.5*self.dl + self.beg*self.dl + (l+1)*self.dl):
                        k = k + 1
                        layer[l] = layer[l] + b.state.vel[self.index]
                if k>0: layer[l] = layer[l] / k
            self.space_time_array = np.vstack((self.space_time_array, layer))
            self.N_t = self.N_t + 1
            if (self.verbose): print(clr.END + clr.BOLD + clr.BEIGE + "Checkpoint UFFT data")

        def final_dft(self, AUX):
            if (self.verbose): print(clr.END + clr.BOLD + clr.BEIGE + "UFFT computations")
            self.space_time_array = np.delete(self.space_time_array, 0, 0) # remove initial zero layer
            self.space_time_array = np.transpose(self.space_time_array)
            # Spatial fft
            k_time_array = np.zeros([self.N_l//2, self.N_t, 2])
            for j in range(self.N_t):
                k_time_array[:,j, 0] = fft(self.space_time_array[:,j]).real[0:self.N_l//2]
                k_time_array[:,j, 1] = fft(self.space_time_array[:,j]).imag[0:self.N_l//2]

            # Temporal fft
            k_omega_array = np.zeros([self.N_l//2, self.N_t//2, 2])
            for j in range(self.N_l//2):
                k_omega_array[j,:,0] = ifft(k_time_array[j,:,0] + 1j * k_time_array[j,:,1]).real[0:self.N_t//2]
                k_omega_array[j,:,1] = ifft(k_time_array[j,:,0] + 1j * k_time_array[j,:,1]).imag[0:self.N_t//2]

            # Spectral density
            S_k_omega = np.zeros([self.N_t//2, self.N_l//2])
            for i in range(self.N_l//2):
                for j in range(self.N_t//2):
                    S_k_omega[j,i] = k_omega_array[i,j,0]**2 + k_omega_array[i,j,1]**2

            # Save spectral density
            path = os.getcwd() + self.dir
            if ( not os.path.exists( path )):
                os.mkdir( path )

            path = path + self.run + '/'
            if ( not os.path.exists( path )):
                os.mkdir( path )

            path = path + 'ufft/'
            if ( not os.path.exists( path )):
                os.mkdir( path )

            k_min = 2 * np.pi /( self.dl * self.N_l)
            k_max = 2 * np.pi /( self.dl)

            w_min = 2 * np.pi /( self.dt * self.N_t)
            w_max = 2 * np.pi /( self.dt)

            extent = [k_min,k_max,w_min,w_max]

            np.save(path + "spectral.npy", [S_k_omega, extent])
            np.save(path + "space_time.npy", self.space_time_array)
            if (self.verbose):
                print(clr.END + clr.BOLD + clr.RED + "Final DFT computations done")

            print("N_l = ", self.N_l)
            print("N_t = ", self.N_t)

            S_kwt = np.transpose(S_k_omega)
            N = np.shape(S_kwt)[0]

            print("number of columns", N)

            NOR = np.sum(S_kwt[:,:])
            nn = N//5 # Five frequency bands
            print("number of columns in a band", nn)
            bands = np.zeros([5])
            bands[0] = np.sum(S_kwt[0:nn     ,:]) / NOR
            bands[1] = np.sum(S_kwt[nn:2*nn  ,:]) / NOR
            bands[2] = np.sum(S_kwt[2*nn:3*nn,:]) / NOR
            bands[3] = np.sum(S_kwt[3*nn:4*nn,:]) / NOR
            bands[4] = np.sum(S_kwt[4*nn:5*nn,:]) / NOR

            return bands
