### make RVE.py

# This script creates an RVE according to the description in RVE dictionary

import numpy as np
from generatearrangement import *
from creatematerials import *
from setengines import *
from yade import Vector3
import os.path
import time
from color_lib import clr

def makeRVE(O, RVE, MAT, OPT, pl):

    # General RVE parameters
    margin = RVE['margin']
    sigmaIso=RVE['sigmaIso']
    damp = RVE['damping']


    if (RVE['type'] == 'FCC_layer') or (RVE['type'] == 'FCC_rand'):

        props = RVE['props']
        N_pack = [props['N_x'], props['N_y'], props['N_z'], props['N_b'], props['N_c'], props['N_d']]
        reg_box_x = props['subcell_size'] * (1 + margin ) * props['N_x']
        reg_box_y = props['subcell_size'] * (1 + margin ) * props['N_y']
        reg_box_z = props['subcell_size'] * (1 + margin ) * props['N_z']
        reg_box = [reg_box_x, reg_box_y, reg_box_z]
        reg_type = props['type']
        subcell_size = props['subcell_size']

    O = createMaterials(MAT, O)
    O.periodic = True

    if (RVE['type'] == 'FCC_layer'):
        O.cell.setBox(reg_box)
        O = generateFCCPhonon(O, N_pack, subcell_size, reg_type, margin, MAT)

    if (RVE['type'] == 'FCC_rand'):
        O.cell.setBox(reg_box)
        O = generateFCCRandom(O, N_pack, subcell_size, reg_type, margin, MAT, RVE['placement'])

    # Temporary fix all DOFs
    for b in O.bodies: b.state.blockedDOFs='xyzXYZ'
    O = set_engines_peri(RVE, MAT, O); O.step()

    # reset equilibrium distance to avoid locking stress
    #for b in O.interactions: b.phys.unp=b.geom.penetrationDepth
    #O.step()
    # release DOFs
    for b in O.bodies: b.state.blockedDOFs=''

    O.dt=RVE['RVE_TS']
    if (OPT['verbose']):
        print(clr.END + clr.BOLD + clr.BLUE + "Running isotropic compression", clr.END + clr.BOLD + clr.YELLOW, "\n Goal stress: (", '{0:10.2e}'.format(sigmaIso), ",", '{0:10.2e}'.format(sigmaIso), ",", '{0:10.2e}'.format(sigmaIso), ") Pa")
        print(clr.END + clr.BOLD + clr.WHITE + "--------------------------------------------------------------------------------")

    O.run(-1, True)


    if (OPT['verbose']): print(clr.END + clr.BOLD + clr.GREEN + "RVE created successfully")
    return O

