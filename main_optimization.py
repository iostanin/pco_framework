### main_optimization.py

##----------------------------------------------------------
##      Phononic Crystal Optimization (PCO) Framework
##
## Igor Ostanin, University of Twente, i.ostanin@utwente.nl
##----------------------------------------------------------

# This script performs the simulation-driven
# multiparameteric optimization of the phononic
# crystal structure.
# Major simulation routine is given in pipeline.py
# Goal functionals are described in functional.py
# The simulation parameters are at baseline.py
# This file describes optimization problem statement
# and runs the optimization procedure


#----------Import modules-----------------------------------
import numpy as np
import sys
import getpass
import time
user = getpass.getuser()
sys.path.append(os.getcwd())
from color_lib import clr
from u_fft import ufftSolver
from fft import dftSolver
from pipeline import *
from baseline import *
from scipy.optimize import minimize
#-----------------------------------------------------------

##----------------------------------------------------------
## 1) Initialization
##----------------------------------------------------------

# Baseline parameters
RVE, MAT, TEST, OPT, AUX = phononic_test_baseline()

# Optimization preferred options
OPT['wdir'] = '/optimization/'
OPT['verbose'] = True
OPT['peakMon'] = False
OPT['iterInfo'] = True

# Zero state path
zeropath = os.getcwd() + OPT['wdir']
if ( not os.path.exists( zeropath )):
    os.mkdir( zeropath )
O.save(zeropath + 'zero.yade.gz')
OPT['zeropath'] = zeropath

# Initialize Qt objects for Qt visualization
pl = False
if (OPT['plot']):
    rr = qt.Renderer()
    vv = qt.View()
    cc = qt.Controller()
    pl = [rr, vv, cc]


# Create all-particle FFT solver
dft = False
if (OPT['saveDFT']):
    dft = dftSolver(O,TEST,OPT)

# Create layer-wise FFT solver
ufft = False
if (OPT['saveUFFT']):
    ufft = ufftSolver(OPT)


##----------------------------------------------------------------
## Optimization function wrapper
##----------------------------------------------------------------

# Global counter of pipeline runs
i = 0

# Baseline wrapper: z are optimized args, params are constant args

fun_batch = False
den_batch = False

def fun1 (z, params):
    global i, fun_batch, par_batch, den_batch, dft
    O, RVE, MAT, TEST, OPT, AUX, pl = params

    d_max = MAT['max_dens']
    d_min = MAT['min_dens']

    densities = (d_max + d_min)/2 - ((d_max - d_min)/2) * np.cos(z)
    if (OPT['iterInfo']): print(clr.BLUE2 + " Densities: ", densities)

    for k in range(len(densities)):
        MAT['mat_array'][k+1]['dens'] = densities[k]

    OPT['nrun'] = 'run_' + str(i)
    ret = pipelineRun(O, RVE, MAT, TEST, OPT, AUX, pl, dft, ufft)[7] # 1st band (FFT) optimization

    if i==0:
        fun_batch = ret
        den_batch = densities
    else:
        fun_batch = np.append(fun_batch, ret)
        den_batch = np.vstack([den_batch, densities])
        np.savetxt(zeropath + 'fun.txt', fun_batch, delimiter=',')
        np.savetxt(zeropath + 'densities.txt', den_batch, delimiter=',')

    if (OPT['iterInfo']): print(clr.END + clr.BOLD + clr.VIOLET + "Iteration #: ", i,", \n Input parameters: ", z , " \n Functional value: ", ret)

    i = i + 1

    return ret # Maximization of energy in the band

# Interface for scipy minimize
params = [O, RVE, MAT, TEST, OPT, AUX, pl]

def fun3(x): return fun1 (x, params)

#Initial guess and bounds
x0 = 0.5 * np.pi * np.ones(RVE['props']['N_c'])
# Randomization
#for kk in range(len(x0)):
#    x0[kk] = np.pi * np.random.rand()


if (OPT['iterInfo']):
    print(clr.END + clr.BOLD + clr.VIOLET + "--------------------------------------------------------------------------------")
    print(clr.END + clr.BOLD + clr.VIOLET + "                 ENTERING OPTIMIZATION ROUTINE                      ")
    print(clr.END + clr.BOLD + clr.VIOLET + "--------------------------------------------------------------------------------")

fun3(x0) # Zero run
res = minimize(fun3, x0, method='Powell', options = {'xtol':1e-3, 'ftol':1e-4})

if res['success']:
    print(clr.END + clr.BOLD + clr.VIOLET + "  \n\n      Optimization converged sucessfully! \n\n")
    print(clr.END + clr.BOLD + clr.VIOLET + "  Resulting set of input parameters: \033[1;37;40m", res['x'])
    print(clr.END + clr.BOLD + clr.VIOLET + "  Resulting functional value: \033[1;37;40m", res['fun'])
    print(clr.END + clr.BOLD + clr.VIOLET + "  Number of interations: \033[1;37;40m", res['nfev'], "\n\n")


if (OPT['iterInfo']):
    print(clr.END + clr.BOLD + clr.VIOLET + "--------------------------------------------------------------------------------")
    print(clr.END + clr.BOLD + clr.VIOLET + "                   OPTIMIZATION ROUTINE DONE                                    ")
    print(clr.END + clr.BOLD + clr.VIOLET + "--------------------------------------------------------------------------------")




