### waveaux.py
import os.path
from color_lib import clr
# This file contains a set of auxiliary functions used in the simulation
from yade import Vector3

def nth(i,N): return (i/N==i//N) # True if i = N * integer (python 3 only)

def init_test(O, geometry):
    # Fix grips
    for b in geometry[0] + geometry[1]:
        b.state.blockedDOFs = 'xyzXYZ'
        b.state.vel = Vector3(0.,0.,0.)
    # Calm down remaining oscillations
    for b in O.bodies:
        b.state.vel = Vector3(0, 0, 0)
    return O

def agitateWave(O, geometry, TEST, OPT):
        import numpy as np

        from yade import Vector3
        grip1 = geometry[0]
        agitationSteps = TEST['agitationSteps']
        timeStep = O.dt
        omega = 2 * np.pi / (agitationSteps * timeStep)
        sType = TEST['waveShape']
        A = TEST['waveMag']
        t = timeStep * O.iter
        if sType == 'sin': vv = - A * omega * np.cos(omega*t)
        if sType == 'cos': vv = - A  * omega * np.sin(omega*t)
        if sType == 'lin': vv = A / agitationSteps * timeStep
        # apply displacements
        if TEST['waveType'] == 'P':
            for s in grip1:
                if (O.iter<agitationSteps):
                    s.state.vel = Vector3(0,0,vv)
                if (O.iter==agitationSteps):
                    s.state.vel = Vector3(0, 0, 0)
        if TEST['waveType'] == 'S':
            for s in grip1:
                if (O.iter<agitationSteps):
                    s.state.vel = Vector3(vv,0, 0)
                if (O.iter==agitationSteps):
                    s.state.vel = Vector3(0, 0, 0)


def savePropData(O, OPT):
        from yade import export
        import numpy as np
        if (OPT['saveVTK']):
            if (OPT['verbose']): print(clr.END + clr.BOLD + clr.YELLOW + "Saving vtk configurations" )

            path = os.getcwd() + OPT['wdir']
            if ( not os.path.exists( path )):
                os.mkdir( path )

            path = path + OPT['nrun'] + '/'
            if ( not os.path.exists( path )):
                os.mkdir( path )

            path = path + 'vtk/'
            if ( not os.path.exists( path )):
                os.mkdir( path )

            vtkExporter = export.VTKExporter(path)
            vtkExporter.exportSpheres(numLabel = O.iter, what = dict( \
                                      dist = 'b.state.pos.norm()', \
                               linVelocity = 'b.state.vel', \
                               angVelocity = 'b.state.angVel', \
                               mass = 'b.state.mass', \
                               mat_rand = 'b.material.id', \

                             numOfContacts = 'len(b.intrs())'))

def finalSave(OPT, AUX, dft, ufft):
        AUX = saveState(OPT, AUX, dft, ufft)
        return AUX

def saveState(OPT, AUX, dft, ufft):
    O.pause()
    if (OPT['saveStates']):
        path = os.getcwd() + OPT['wdir']
        if ( not os.path.exists( path )):
            os.mkdir( path )
        path = os.getcwd() + OPT['wdir'] + OPT['nrun'] + '/'
        if ( not os.path.exists( path )):
            os.mkdir( path )
        O.save( path + OPT['nrun'] + '_final.yade.gz')
        if (OPT['verbose']): print(clr.END + clr.BOLD + clr.YELLOW, "Final state saved")
    if OPT['saveDFT']:
        bands = dft.final_dft(AUX)
        AUX['band_1d'] = bands[0]
        AUX['band_2d'] = bands[1]
        AUX['band_3d'] = bands[2]
        AUX['band_4d'] = bands[3]
        AUX['band_5d'] = bands[4]
    if OPT['saveUFFT']:
        bands = ufft.final_dft(AUX)
        AUX['band_1f'] = bands[0]
        AUX['band_2f'] = bands[1]
        AUX['band_3f'] = bands[2]
        AUX['band_4f'] = bands[3]
        AUX['band_5f'] = bands[4]
    return AUX


# This function gets geometry of gage and grips
def setGage(O, thickness):
    from yade.utils import aabbExtrema
    min, max = aabbExtrema()

    grip1 = [b for b in O.bodies if b.state.pos[2] <= min[2] + thickness]
    grip2 = [b for b in O.bodies if b.state.pos[2] >= max[2] - thickness]

    for b in grip1: b.shape.color = [1.0, 0.0, 0.0]
    for b in grip2: b.shape.color = [0.0, 0.0, 1.0]

    gage = [b for b in O.bodies if ((b not in grip1) and (b not in grip2))]
    return [grip1, grip2, gage]


def VelocityProfile(O, TEST, OPT):
    import numpy as np
    from yade.utils import setRefSe3, aabbExtrema

    min, max = aabbExtrema()
    gag = max[2] - min[2]
    lay = 2 * np.mean([b.shape.radius for b in O.bodies])
    N_layers = int (gag/lay)
    profile = np.zeros(N_layers)

    z  = np.array([b.state.pos[2] for b in O.bodies]);
    if (TEST['waveType']=='P'):
        vz = np.array([b.state.vel[2] for b in O.bodies]);
    if (TEST['waveType']=='S'):
        vz = np.array([b.state.vel[0] for b in O.bodies]);


    for i in range(N_layers):
        beg = min[2] + i * lay
        end = min[2] + (i+1) * lay
        k = 0
        for ii in range(len(z)):
            if ((z[ii]>beg) and (z[ii]<end)):
                profile[i] = profile[i] + vz[ii]
                k = k + 1
        profile[i] = profile[i] / k
    return profile, lay, N_layers

# This function averages velocity profile along z-direction
def saveVelocityProfile(O, TEST, OPT):
    import numpy as np
    profile, lay, N_layers = VelocityProfile(O, TEST, OPT)
    if (OPT['saveProf']):
        path = os.getcwd() + OPT['wdir']
        if ( not os.path.exists( path )):
            os.mkdir( path )
        path = path + OPT['nrun'] + '/'
        if ( not os.path.exists( path )):
            os.mkdir( path )
        path = path + 'profile/'
        if ( not os.path.exists( path )):
            os.mkdir( path )
        np.savetxt(path + 'profile'+str(O.iter)+'.txt', profile, delimiter=',')
        np.save(path + 'profile'+str(O.iter)+'.npy', profile)
        # Update info on the number of files and their length
        N_snaps = int(O.iter / TEST['saveEvery'])
        np.save(path + 'info.npy', [N_snaps, N_layers])


    return 0


def addPosMagSample(O, TEST, OPT, AUX):
    import numpy as np
    from yade.utils import aabbExtrema

    profile, lay, _ = VelocityProfile(O, TEST, OPT)
    mask = np.zeros(len(profile), dtype = bool)


    vmax  = np.amax(profile)
    imax = np.where(profile == np.amax(profile))[0]

    if imax >= len(profile)-1:
        AUX['isPeak'] = False

    if imax < len(profile)-1:
        if AUX['isPeak'] == True:
            # Compute velocity and magnitude using quadratic interpolation
            x1 = int(imax - 1); x2 = int(imax); x3 = int(imax + 1);
            if (OPT['peakMon'] and OPT['verbose']): print("Peak x-s:", x1, x2, x3)
            y1 = profile[x1]; y2 = profile[x2]; y3 = profile[x3];
            if (OPT['peakMon'] and OPT['verbose']): print("Peak y-s:", y1, y2, y3)
            a = np.array([[x1*x1,x1,1], [x2*x2,x2,1], [x3*x3,x3,1]])
            y = np.array([y1,y2,y3])
            v = np.matmul(np.linalg.inv(a),y)
            smax = - 0.5 * v[1]/v[0]
            mmax = v[0] * smax * smax + v[1] * smax + v[2]
            if (OPT['peakMon'] and OPT['verbose']): print("Peak position = ", smax, ", peak magnitude = ", mmax, ", time = ", TEST['EX_TS'] * O.iter)
            AUX['SamplesCollected'] = AUX['SamplesCollected'] + 1
            AUX['peakPosition'] = np.append(AUX['peakPosition'], smax * lay)
            AUX['peakMagnitude'] = np.append(AUX['peakMagnitude'], mmax)
            AUX['peakMoment'] = np.append(AUX['peakMoment'], TEST['EX_TS'] * O.iter)

    return 0


# This function computed velocity and attenuation with LS
def computeVelMag(O, AUX, OPT):
    import numpy as np

    N = AUX['SamplesCollected']
    if (OPT['peakMon'] and OPT['verbose']): print ('Samples collected =', N)

    x = AUX['peakMoment']
    X = np.mean(x)

    y = AUX['peakPosition']
    Y = np.mean(y)
    num = 0; den = 0;
    for i in range (len(x)):
        num = num + (x[i]-X)*(y[i]-Y)
        den = den + (x[i]-X)*(x[i]-X)
    Vel = num/den

    y = AUX['peakMagnitude']
    Y = np.mean(y)
    num = 0; den = 0;
    for i in range (len(x)):
        num = num + (x[i]-X)*(y[i]-Y)
        den = den + (x[i]-X)*(x[i]-X)
    Q = num/den

    if (OPT['peakMon'] and OPT['verbose']): print("Velocity = ", Vel)
    if (OPT['peakMon'] and OPT['verbose']): print("Attenuation = ", Q)

    # Reset Auxiliary variables
    AUX['isPeak'] = True
    AUX['SamplesCollected'] = 0
    AUX['peakPosition'] = np.array([0.0])
    AUX['peakMagnitude'] = np.array([0.0])
    AUX['peakMoment'] = np.array([0.0])

    if (OPT['saveProf']):
        path = os.getcwd() + OPT['wdir']
        if ( not os.path.exists( path )):
            os.mkdir( path )
        path = path + OPT['nrun'] + '/'
        if ( not os.path.exists( path )):
            os.mkdir( path )
        path = path + 'profile/'
        if ( not os.path.exists( path )):
            os.mkdir( path )
        np.save(path + 'vel.npy', Vel)

    return Vel, Q


def computeVelMag1(O, TEST, OPT):
    import numpy as np
    from yade.utils import setRefSe3, aabbExtrema

    min, max = aabbExtrema()
    gag = max[2] - min[2]
    lay = 2 * np.mean([b.shape.radius for b in O.bodies])
    N_layers = int (gag/lay)
    profile = np.zeros(N_layers)

    z  = np.array([b.state.pos[2] for b in O.bodies]);
    vz = np.array([b.state.vel[2] for b in O.bodies]);

    for i in range(N_layers):
        beg = min[2] + i * lay
        end = min[2] + (i+1) * lay
        k = 0
        for ii in range(len(z)):
            if ((z[ii]>beg) and (z[ii]<end)):
                profile[i] = profile[i] + vz[ii]
                k = k + 1
        profile[i] = profile[i] / k

    # Initial velocity

    Mag0 = TEST['waveMag'] * 2 * np.pi / ( TEST['agitationSteps'] * TEST['EX_TS'] )

    vmax  = np.amax(profile)
    imax = np.where(profile == np.amax(profile))[0]

    # Compute velocity and magnitude in a piecewise-constant approximation
    Vel = (int(imax) * lay / (TEST['EX_TS'] * O.iter))
    Mag = vmax
    if (OPT['peakMon'] and OPT['verbose']): print("velocity = ", Vel, "Mag0 = ", Mag0,  "Mag = ", Mag)


    return Vel, Mag


def addKineticEnergy(O, RVE, AUX):

    props = RVE['props']
    [Nz, Nb, Nc, Nd] = [props['N_z'], props['N_b'], props['N_c'], props['N_d']]

    # Locate detector - take into account that the column was compressed
    from yade.utils import aabbExtrema
    min, max = aabbExtrema()
    cc = (max[2] - min[2]) / Nz
    beg = min[2] + cc * ( 1 + Nb + Nc )
    end = min[2] + cc * ( 1 + Nb + Nc + Nd)

    # Sum kinetic energy of detector particles
    for b in O.bodies:
        if b.state.pos[2] > beg and b.state.pos[2]<end:
            m = b.state.mass
            v = b.state.vel.norm()
            AUX['kinetic_integral'] = AUX['kinetic_integral'] + 0.5*m*v*v


def normalizeKineticEnergy(AUX):
    # Normalization
    if AUX['kinetic_integral_0']==1.0:
        AUX['kinetic_integral_0'] = AUX['kinetic_integral']; AUX['kinetic_integral'] = 1.0
    else:
        AUX['kinetic_integral'] = AUX['kinetic_integral'] / AUX['kinetic_integral_0']






